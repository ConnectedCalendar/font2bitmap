#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

const char font[] = "open-sans-bold";
//const char size = 50; // heading
//const char type = 'h';
const char size = 38; // text
const char type = 't';

const char command[] =
"cd font && convert +antialias -font %s -pointsize %d label:\"\\%c\" %c%u.xbm";
const char commandRN[] =
"cd font && convert +antialias -font %s -pointsize %d label:%c %c%u.xbm";
const char commandBS[] =
"cd font && convert +antialias -font %s -pointsize %d label:'\\%c' %c%u.xbm";
const char buffer[200];


int main() {
    // go through ASCII table
    for (uint8_t c = 32; c < 175; ++c) {
        if ((c == 'r') || (c == 'n')) {
            sprintf(buffer, commandRN, font, size, c, type, c);
            system(buffer);
        } else if (c == 92) {
            sprintf(buffer, commandBS, font, size, c, type, c);
            system(buffer);
        } else {
            sprintf(buffer, command, font, size, c, type, c);
            system(buffer);
        }
    }

    return 0;
}